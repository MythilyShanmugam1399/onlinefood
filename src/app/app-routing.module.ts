import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminAccessComponent } from './admin-access/admin-access.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { BodyComponent } from './body/body.component';
import { CartComponent } from './cart/cart.component';
import { ContentComponent } from './content/content.component';
import { PaymentComponent } from './payment/payment.component';
import { SearchComponent } from './search/search.component';
import { TrackOrderComponent } from './track-order/track-order.component';

const routes: Routes = [
  {path:"", redirectTo:"/adigas",pathMatch:"full"},
  {path:"admin",component:AdminPageComponent},
  {path:"cart" , component: CartComponent},
  {path:"cart/payment" , component: PaymentComponent},
  {path:"search/:name",component:SearchComponent},
  {path:"cart/payment/trackOrder",component:TrackOrderComponent},
  {path:"accessPage",component:AdminAccessComponent},
  {path:":name" , component: ContentComponent},
  //{path:"rotiGhar" , component: ContentComponent},
  //{path:"punjabiRasoi" , component: ContentComponent},
  //{path:"udup" , component: ContentComponent},
  //{path:"Haunted" , component: ContentComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
