import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  @Input() search:string;
  constructor() { 
    this.search='';
    console.log(this.search);
  }

  ngOnInit(): void {
    console.log(this.search);
  }

}
