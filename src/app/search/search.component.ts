import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestaurantService } from 'src/service/restaurantservice.service';
import { UserService } from 'src/service/user.service';
import { Cart } from '../model/cart';
import { Menu } from '../model/Restaurant';
import { searchShare } from '../shared/searchShare';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  searchItem:any='';
  menuList:Array<Menu>=[];//menun list

  m:Array<Menu>=[];//temp
  act:Menu=new Menu();
  cart:Cart=new Cart();
  decision:string='';

  display:string='none';
  matDisplay:Menu=new Menu();

  constructor(private router:ActivatedRoute,private menuService:RestaurantService,
    private userService:UserService,private search:searchShare) { }

  ngOnInit(): void {

    this.router.paramMap.subscribe(result=>{
      let name = result.get('name');
      this.searchItem=name;
    this.menuService.getMenuFromSearch(this.searchItem).subscribe(data=>{
      this.menuList=data;
    })
  });

  }

  addToCart(menu:Menu){

    let tempCart:Cart = this.search.getCart();

    if(tempCart.cartId==0){
      alert("Please login to access the application");
    }else{

    this.userService.getByCartId(tempCart.cartId).subscribe((data)=>{
      
      let c : Cart = new Cart();
      let total:number=0;
      c.cartId = data.cartId;
      this.m = data.menu;
      this.m.push(menu);
      c.menu = this.m;
      c.quanity = this.m.length;
      for(let i=0;i<this.m.length;i++){
         this.act = this.m[i];
         total=total+this.act.foodPrice;
      }
      c.totalPrice=total;

      this.userService.addToCart(tempCart.cartId,c).subscribe((data)=>{
        console.log(data);
      })
    });

     this.userService.getByCartId(tempCart.cartId).subscribe((data)=>{
       this.search.setCart(data);
     });

  }
    
  }

}
