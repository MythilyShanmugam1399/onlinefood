import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AreaService } from 'src/service/areaservice.service';
import { UserService } from 'src/service/user.service';
import { Address } from '../model/address';
import { Cart } from '../model/cart';
import { CardType } from '../model/cartType';
import { Country } from '../model/country';
import { OrderDetail } from '../model/orderDetail';
import { Payment } from '../model/payment';
import { State } from '../model/state';
import { User } from '../model/user';
import { searchShare } from '../shared/searchShare';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  stateList:Array<State>=[];
  countryList:Array<Country>=[];
  customer:User=new User();
  address:Address=new Address();
  payment:Payment=new Payment();
  visa:any='';
  master:any=''
  orderDetails:OrderDetail=new OrderDetail();

  returnAddress:Address=new Address();
  selectedstate:string='';
  selectedcountry:string='';

  emptyCart:Cart=new Cart();

  buttonDisplay:boolean=false;

  constructor(private service:AreaService,private rout:Router,private share:searchShare,private userService:UserService) { }

  ngOnInit(): void {

    if(this.share.getUser().userId==0){
      alert("please login to access the application");
      this.rout.navigate(['/adigas'])
    }else{
      //To get the list of state
      this.service.getState().subscribe(data=>{
      console.log(data);
      this.stateList=data;
    });
    //To get the list of country
    this.service.getCountry().subscribe(data=>{
    console.log(data);
    this.countryList=data;
  });
    //To get the cart type available
    this.userService.getCardType().subscribe(data=>{
    console.log(data);
    this.visa=data[0];
    this.master=data[1];
  })
    this.customer = this.share.getUser();
    this.userService.getByCartId(this.customer.cart.cartId).subscribe((data)=>{
      this.orderDetails.quantity=data.quanity;
      this.orderDetails.total=data.totalPrice;
    })
  }//else if

}

//To add the order
addToOrder(){

  this.payment.user=this.customer;//payment ready

  this.userService.addPayment(this.payment).subscribe((data)=>{console.log(data)});
  this.orderDetails.address=this.returnAddress;
  this.orderDetails.user=this.customer;//ordetails ready

  this.userService.addOrder(this.orderDetails).subscribe((data)=>{console.log(data);
  this.share.setOrder(data);
  this.buttonDisplay=true;
  });

  let c = new Cart();

  this.userService.createCart(c).subscribe((data)=>{
    this.customer.cart=data;
    this.userService.updateCartToUser(this.customer.userId,this.customer).subscribe((data)=>{console.log(data)})
  })

}

//To add the address to delivery
saveChanges(){

  for(let i=0;i<this.stateList.length;i++){
    if(this.stateList[i].stateName===this.selectedstate){
      this.address.state=this.stateList[i];
      break;
    }
  }

  for(let i=0;i<this.countryList.length;i++){
    if(this.countryList[i].countryName===this.selectedcountry){
      this.address.country=this.countryList[i];
      break;
    }
  }

  if(this.address.state==null){alert("please select the state")}else
  if(this.address.country==null){alert("please select the country")}else{
  this.address.user=this.customer;//address ready
  this.userService.addAddress(this.address).subscribe((data)=>{
  this.returnAddress=data;
  });

  }
}

}

