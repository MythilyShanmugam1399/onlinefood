import { Injectable } from '@angular/core';
import { HeaderComponent } from '../header/header.component';
import { Cart } from '../model/cart';
import { OrderDetail } from '../model/orderDetail';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class searchShare{
   
    searchItem:string='';
    cart:Cart=new Cart();
    user:User=new User();
    order:OrderDetail=new OrderDetail();
    
    setSearchItem(msg:string){
        this.searchItem=msg;
        console.log(this.searchItem);
    }
    getSearchItem(){
        return this.searchItem;
    }
    setCart(cart:Cart){
      this.cart=cart;
    }
    getCart(){
        return this.cart;
    }
    setUser(user:User){
        this.user=user;
    }
    getUser(){
        return this.user;
    }
    setOrder(order:OrderDetail){
        this.order=order;
        console.log(this.order);
    }
    getOrder(){
        return this.order;
    }
}