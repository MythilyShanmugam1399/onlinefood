import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from 'src/service/admin.service';
import { Admin } from '../model/admin';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {

  email:string='';
  password:string='';
  admin:Admin=new Admin();
  validationError:string='';
  constructor(private service:AdminService,private rout:Router) { }

  ngOnInit(): void {
    
  }
  
  //To validate the admin credential
  loginValidate(){

    this.service.getAdmin(this.email).subscribe((data)=>{
      this.admin=data;

      if(this.email===this.admin.email && this.password===this.admin.password){
        this.validationError='Successfully logged in';
        this.rout.navigate(["/accessPage"]);
      }else{
        this.validationError='UserName and Password are incorrect';
      }

  },(e)=>{
    this.validationError='User not Found';
  });

  }
  

}
