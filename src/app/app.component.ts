import { Component, EventEmitter, Input } from '@angular/core';
import { Cart } from './model/cart';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title:string;
  @Input() cart = new Cart();
  constructor(){
    this.title="My App";
  }
  getMessage(data:any){
    this.cart=data;
    console.log(this.cart);
  }
 
}
