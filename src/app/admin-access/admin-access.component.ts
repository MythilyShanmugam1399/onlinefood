import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/service/admin.service';
import { RestaurantService } from 'src/service/restaurantservice.service';
import { Restaurant } from '../model/Menu';
import { Menu } from '../model/Restaurant';

@Component({
  selector: 'app-admin-access',
  templateUrl: './admin-access.component.html',
  styleUrls: ['./admin-access.component.css']
})
export class AdminAccessComponent implements OnInit {

  res:Restaurant=new Restaurant();
  food:Menu=new Menu();
  photoPreview:boolean=false;
  resList:Restaurant[]=[];
  selectedRestaurant:string='';
  msg:string='';

  constructor(private service:AdminService,private resService:RestaurantService) { }

  ngOnInit(): void {

    //By default to get the restaurant list to display
   this.resService.getRestaurant().subscribe((data)=>{
     console.log(data);
     this.resList=data;
   })

  }

  addRestaurant(){

    //To register the restaurant
    this.service.addRestaurant(this.res).subscribe((data)=>{console.log(data)
      this.res=data;
    })

  }

  addFood(){

    console.log(this.selectedRestaurant);
    for(let i=0;i<this.resList.length;i++){

      if(this.resList[i].resName===this.selectedRestaurant){
        this.food.restaurant=this.resList[i];
        break;
      }

    }
    //Add food to restaurant 
    this.service.addFood(this.food).subscribe((data)=>{console.log(data);
     this.msg="added";
  });

  }

  displayImage(){

    //Photo preview
    this.photoPreview=!this.photoPreview;

  }

}
