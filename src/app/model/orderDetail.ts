import { Address } from "./address";
import { User } from "./user";
export class OrderDetail{
    orderId:number=0;
    "quantity":number;
    "total":number;
    "address":Address;
    "user":User;
}