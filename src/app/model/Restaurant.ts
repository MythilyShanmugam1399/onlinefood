import { Restaurant } from "./Menu";

export class Menu{
"foodId":number;
"foodCategory":string;
"foodName":string;
"foodPrice":number;
"imageURL":string;
"restaurant":Restaurant={
    "resId":0,
    "resName":'',
    "resLocation":''
};
}
