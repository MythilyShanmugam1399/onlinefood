import { Country } from "./country";
import { State } from "./state";
import { User } from "./user";

export class Address{
    "addressId":number;
    "streetName":string;
    "houseNum":string;
    "city":string;
    "pincode":number;
    "state":State;
    "country":Country;
    "user":User;
}