import { Cart } from "./cart";
import { CardType } from "./cartType";
import { User } from "./user";

export class Payment{
    paymentId:number=0;
    "user":User;
    nameHolder:string='';
    cardType:CardType={"cardTypeId":0,"cardName":''};
    cardNum:string='';
    "securityCode":number;
    "month":number;
    "year":number;
}