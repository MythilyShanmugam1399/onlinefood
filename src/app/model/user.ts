import { Cart } from "./cart";

export class User{
    userId:number=0;
    userFirstName:string='';
    userLastName:string='';
    userEmail:string='';
    password:string='';
    cart:Cart={
        cartId:0,
        quanity:0,
        totalPrice:0,
        menu:[]
    }
}