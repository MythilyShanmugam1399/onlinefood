import { Component,EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, UrlSegment } from '@angular/router';
import { RestaurantService } from 'src/service/restaurantservice.service';
import { UserService } from 'src/service/user.service';
import { Cart } from '../model/cart';
import { Menu } from '../model/Restaurant';
import { User } from '../model/user';
import { searchShare } from '../shared/searchShare';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  customer:User=new User();
  hideSuccessMessage :boolean=true;
  menuList:Array<Menu>=[];//menu list
  m:Array<Menu>=[];//temp
  act:Menu=new Menu();
  cart:Cart=new Cart();
  decision:string='';

  display:string='none';
  matDisplay:Menu=new Menu();

  quantity:number=0;
  total:number=0;

  constructor(private router:ActivatedRoute,private search:searchShare,
    private menuService:RestaurantService,private userService:UserService) { }

  ngOnInit(): void {

    this.customer=this.search.getUser();

    this.router.url.subscribe((url:UrlSegment[])=>{this.decision = url[0].path.toString();
      this.menuService.getMenuList(this.decision).subscribe(data=>{
        this.menuList=data;
      })
    });

  }

  //To add the items to cart
  addToCart(menu:Menu){
    
    let tempCart:Cart = this.search.getCart();

    if(tempCart.cartId==0){
      alert("Please login to access the application");
    }else{

    this.userService.getByCartId(tempCart.cartId).subscribe((data)=>{
      
      let c : Cart = new Cart();
      let total:number=0;

      c.cartId = data.cartId;
      this.m = data.menu;
      this.m.push(menu);
      c.menu = this.m;
      c.quanity = this.m.length;
      for(let i=0;i<this.m.length;i++){
         this.act = this.m[i];
         total=total+this.act.foodPrice;
      }
      c.totalPrice=total;

      this.userService.addToCart(tempCart.cartId,c).subscribe((data)=>{
        console.log(data);
      });

    });

     this.userService.getByCartId(tempCart.cartId).subscribe((data)=>{
       this.search.setCart(data);
       //Calling the fade out method
       this.FadeOutSuccessMsg();
       this.quantity=data.quanity+1;
       this.total=data.totalPrice+menu.foodPrice;
     
     });
  }
    
  }

  //to open the modal
  openMat(obj:Menu){
    this.matDisplay=obj;
    this.display='block';
  }

  //to close the modal
  closeMat(){
    this.display='none';
  }

  //To disappear the msg after 2 sec
  FadeOutSuccessMsg() {
    setTimeout( () => {
           this.hideSuccessMessage = true;
        }, 2000);
        this.hideSuccessMessage = false;
   }

}
