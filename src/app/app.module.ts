import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { BodyComponent } from './body/body.component';
import { FooterComponent } from './footer/footer.component';
import { BodySliderComponent } from './body-slider/body-slider.component';
import { SliderImage } from './model/slider.image';
import { ContentComponent } from './content/content.component';
import { SearchComponent } from './search/search.component';
import { CartComponent } from './cart/cart.component';
import { PaymentComponent } from './payment/payment.component';
import { TrackOrderComponent } from './track-order/track-order.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { AdminAccessComponent } from './admin-access/admin-access.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BodyComponent,
    FooterComponent,
    BodySliderComponent,
    ContentComponent,
    SearchComponent,
    CartComponent,
    PaymentComponent,
    TrackOrderComponent,
    AdminPageComponent,
    AdminAccessComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
