import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router, UrlSegment } from '@angular/router';
import { RestaurantService } from 'src/service/restaurantservice.service';
import { UserService } from 'src/service/user.service';
import { Cart } from '../model/cart';
import { Restaurant } from '../model/Menu';
import { User } from '../model/user';
import { searchShare } from '../shared/searchShare';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  cartdata:any;
  cart:Cart=new Cart();
  res:Restaurant[] = [];

  customer:User = new User();
  email:string='';
  password:string='';
  validationError:string='';
  
  createAccount:string;
  loggingTitle:string;
  userDetail:string;
  logoutTitle:string;
  validity:boolean=false;//register Button

  searchElement:string='';

  constructor(private rout:Router,private router:ActivatedRoute,
    private search:searchShare,private service:UserService,private resService:RestaurantService) {

    this.cartdata='';
    this.createAccount='';
    this.loggingTitle='';
    this.userDetail='';
    this.logoutTitle='';

   }

  ngOnInit(): void {

    this.customer=this.search.getUser();
    if(this.search.getUser().userId==0){
    this.createAccount='Create Account';
    this.loggingTitle='Login';
    }
    this.resService.getRestaurant().subscribe((data)=>{
      this.res=data;
    });

  }

  //To register the cutomer
  saveCustomer(customer:User){

    this.service.addUser(customer).subscribe((data)=>{console.log(data)});
    this.rout.navigate(['/adigas']);
    this.createAccount='';
    this.validity=true;

  }

  //Validate the user login credential
  loginValidater(){
    this.service.getUserForValidation(this.email).subscribe((data)=>{
        this.customer=data;

        if(this.email===this.customer.userEmail && this.password===this.customer.password){
          this.search.setCart(this.customer.cart);
          this.search.setUser(this.customer);
          this.rout.navigate(['/adigas']);
          this.validationError='Successfully logged in';
          this.loggingTitle='';
          this.logoutTitle='Logout';
          this.createAccount='';
        }else{
          this.validationError='UserName and Password are incorrect';
        }

    },(e)=>{
      this.validationError='User not Found';
    });

  }

//Logout button functionality  
showDefault(){

 // this.ngOnInit();
  this.logoutTitle='';
  this.loggingTitle='Login';
  this.createAccount='Create Account';
  this.customer={"userId":0,"userFirstName":'',"userLastName":'',"userEmail":'',"password":'',
  "cart":{"cartId":0,"quanity":0,"totalPrice":0,"menu":[]}};
  this.rout.navigate(['/']);
  this.search.setCart(this.customer.cart);
  this.search.setUser(this.customer);
  
}

//Search the item
performSearch(){

  this.rout.navigate(['/search',this.searchElement]);
  this.searchElement='';

}

}


