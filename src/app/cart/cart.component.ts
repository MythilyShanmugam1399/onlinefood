import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/service/user.service';
import { Cart } from '../model/cart';
import { CartItem } from '../model/cartItem';
import { Menu } from '../model/Restaurant';
import { searchShare } from '../shared/searchShare';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  menuList:Menu[]=[];
  temp:Cart=new Cart();
  total:number=0;
  msg:string='';

  m:Array<Menu>=[];//temp
  act:Menu=new Menu();

  cartItem: CartItem[]=[];

  constructor(private userService:UserService,private rout:Router,private share : searchShare) { }

  ngOnInit(): void {

    this.temp=this.share.getCart();

    //to check whether use logged in 
    if(this.share.getUser().userId==0){
      alert("Please login to access this application");
      this.rout.navigate(['/adigas']);
    }else{
    this.userService.getCartItems(this.temp.cartId).subscribe((data)=>{
    this.cartItem=data;
    });

    //To get the menilist for cart operation
    this.userService.getByCartId(this.temp.cartId).subscribe((data)=>{
      this.menuList = data.menu;
      this.total = data.totalPrice;
    });

  }//if else close

  }

  removeItemFromCart(fid:number){

    this.menuList = this.menuList.filter(obj => obj.foodId !== fid);
    let tempTotal:number=0;

    //based on the food id it remove all the menu with same food id
    let c:Cart = new Cart();
    c.cartId = this.temp.cartId;
    c.menu = this.menuList;
    c.quanity = c.menu.length;
    for(let i=0;i<c.quanity;i++){
      tempTotal+=c.menu[i].foodPrice;
    }
    c.totalPrice=tempTotal;

    this.userService.addToCart(this.temp.cartId,c).subscribe((data)=>{
      this.menuList=data.menu;
      this.ngOnInit();}
      );
      
    }
    incrementToCart(menu:Menu){

      let tempCart:Cart = this.share.getCart();

      //to add the menu to the cart
      this.userService.getByCartId(tempCart.cartId).subscribe((data)=>{
        let c : Cart = new Cart();
        let total:number=0;
        c.cartId = data.cartId;
        this.m = data.menu;
        this.m.push(menu);
        c.menu = this.m;
        c.quanity = this.m.length;
        for(let i=0;i<this.m.length;i++){
           this.act = this.m[i];
           total=total+this.act.foodPrice;
        }
        c.totalPrice=total;

        this.userService.addToCart(tempCart.cartId,c).subscribe((data)=>{
          console.log(data);
          this.ngOnInit();
        });
      });

    }
    decrementFromCart(menu:Menu){

      let tempCart:Cart = this.share.getCart();

      //to decrese the menu from cart
      this.userService.getByCartId(tempCart.cartId).subscribe((data)=>{
        let c :Cart = new Cart();
        let total:number=0;
        c.cartId = data.cartId;
        this.m = data.menu;
        for(let i=0;i<this.m.length;i++){
          if(this.m[i].foodId==menu.foodId){
            this.m.splice(i,1);
            break;
          }
        }
        c.menu=this.m;
        c.quanity=this.m.length;
        for(let i=0;i<this.m.length;i++){
          this.act = this.m[i];
          total=total+this.act.foodPrice;
       }
       c.totalPrice=total;

       this.userService.addToCart(tempCart.cartId,c).subscribe((data)=>{
        console.log(data);
        this.ngOnInit();
      });
      });

    }

}
  

