import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/service/user.service';
import { OrderDetail } from '../model/orderDetail';
import { User } from '../model/user';
import { searchShare } from '../shared/searchShare';

@Component({
  selector: 'app-track-order',
  templateUrl: './track-order.component.html',
  styleUrls: ['./track-order.component.css']
})
export class TrackOrderComponent implements OnInit {

  customer:User=new User();
  orderDetail:OrderDetail=new OrderDetail();

  constructor(private service:UserService,private rout:Router,private shared:searchShare) { }

  ngOnInit(): void {

    if(this.shared.getUser().userId==0){
      alert("Please login to access this application");
      this.rout.navigate(['/adigas']);
    }else{

  this.customer=this.shared.getUser();
  this.orderDetail = this.shared.getOrder();
  
  this.service.getOrderByOrderId(this.orderDetail.orderId).subscribe((data)=>{
    this.orderDetail=data});
    }

  }

}
