import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Admin } from 'src/app/model/admin';
import { Restaurant } from 'src/app/model/Menu';
import { Menu } from 'src/app/model/Restaurant';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private baseUrl = 'http://localhost:8081/adminController/';
  private resBaseUrl = 'http://localhost:8081/restaurantController/';
 
  constructor(private http:HttpClient) { }

  //To get the admin details
  getAdmin(email:string): Observable<Admin>{
    return this.http.get<Admin>(`${this.baseUrl}getAdmin/${email}`);
 }
 
 //Admin can add restaurant
 addRestaurant(res:Restaurant):Observable<Restaurant>{
   return this.http.post<Restaurant>(`${this.resBaseUrl}addRestaurant`,res);
 }

 //Admin can add menu to particular restaurant
 addFood(food:Menu):Observable<Menu>{
   return this.http.post<Menu>(`${this.resBaseUrl}addMenu`,food);
 }

}
