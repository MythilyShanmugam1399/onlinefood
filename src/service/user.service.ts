import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Address } from 'src/app/model/address';
import { Cart } from 'src/app/model/cart';
import { CartItem } from 'src/app/model/cartItem';
import { CardType } from 'src/app/model/cartType';
import { OrderDetail } from 'src/app/model/orderDetail';
import { Payment } from 'src/app/model/payment';
import { User } from 'src/app/model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = 'http://localhost:8081/userController/';

  constructor(private http:HttpClient) { }

  //To register the user
  addUser(user:User): Observable<User>{
    return this.http.post<User>(`${this.baseUrl}addUser`,user);
 }

 //To get the user for login validation
 getUserForValidation(email:string): Observable<User>{
   return this.http.get<User>(`${this.baseUrl}getByEmail/${email}`);
 }

 //Add the items to cart
 addToCart(id:number, cart:Cart): Observable<Cart>{
  return this.http.put<Cart>(`${this.baseUrl}addToCart/${id}`,cart);
}

//Create new cart after checkout
createCart(cart:Cart):Observable<Cart>{
  return this.http.post<Cart>(`${this.baseUrl}createCart`,cart);
}

//Update the cart when user add the item to cart
updateCartToUser(id:number,user:User):Observable<User>{
  return this.http.put<User>(`${this.baseUrl}updateUserCart/${id}`,user);
}

//To get the cart
getByCartId(id:number):Observable<Cart>{
  return this.http.get<Cart>(`${this.baseUrl}getByCartId/${id}`);
}

//To get the card type for select
getCardType():Observable<CardType[]>{
  return this.http.get<CardType[]>(`${this.baseUrl}`+'getCardType');  
}

//To add the address for delivery
addAddress(address:Address): Observable<Address>{
  return this.http.post<Address>(`${this.baseUrl}addAddress`,address);
}

//Add the payment details to proceed
addPayment(payment:Payment): Observable<Payment>{
  return this.http.post<Payment>(`${this.baseUrl}addPayment`,payment);
}

//To add the order
addOrder(order:OrderDetail): Observable<OrderDetail>{
  return this.http.post<OrderDetail>(`${this.baseUrl}addOrder`,order);
}

//To get the cart by user id
getCartByUserId(userId:number):Observable<OrderDetail[]>{
  return this.http.get<OrderDetail[]>(`${this.baseUrl}getOrderByUserId/${userId}`);
}

//To get the order by order id
getOrderByOrderId(id:number):Observable<OrderDetail>{
  return this.http.get<OrderDetail>(`${this.baseUrl}getOrderByOrderId/${id}`);
}

//get the cart items
getCartItems(id:number):Observable<CartItem[]>{
  return this.http.get<CartItem[]>(`${this.baseUrl}getCartItem/${id}`)
}

}
