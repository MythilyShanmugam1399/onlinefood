import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Restaurant } from 'src/app/model/Menu';
import { Menu } from 'src/app/model/Restaurant';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  
  private baseUrl = 'http://localhost:8081/restaurantController/';

  constructor(private http:HttpClient) { }

  //To get the menu list from the restaurant
  getMenuList(name:string): Observable<Menu[]> {  
    return this.http.get<Menu[]>(`${this.baseUrl}getMenuFromRestaurant/${name}`);  
  }

  //To get the result of search item
  getMenuFromSearch(name:string): Observable<Menu[]> {  
    return this.http.get<Menu[]>(`${this.baseUrl}searchMenuFromRestaurant/${name}`);  
  }

  //To get the restaurant for dropdown
  getRestaurant():Observable<Restaurant[]>{
    return this.http.get<Restaurant[]>(`${this.baseUrl}`+'restaurant'); 
  }

}
