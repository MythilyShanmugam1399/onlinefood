import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Country } from 'src/app/model/country';
import { State } from 'src/app/model/state';

@Injectable({
  providedIn: 'root'
})
export class AreaService {

  private baseUrl = 'http://localhost:8081/userController/';
  
  constructor(private http : HttpClient) { }

  //To get the list of state
  getState(): Observable<State[]> {  
    return this.http.get<State[]>(`${this.baseUrl}`+'state');  
  }

  //To get the list of country
  getCountry(): Observable<Country[]> {  
    return this.http.get<Country[]>(`${this.baseUrl}`+'country');  
  }

}
