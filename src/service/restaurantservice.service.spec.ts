import { TestBed } from '@angular/core/testing';

import { RestaurantService } from './restaurantservice.service';

describe('RestaurantserviceService', () => {
  let service: RestaurantService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RestaurantService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
